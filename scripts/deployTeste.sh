if [ $SET_ROLLBACK = "false" ]
then
  docker login -u $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD          
  docker build -t "${IMAGE_REPO}"/${CONTAINER_NAME}:snapshot .
  docker tag "${IMAGE_REPO}"/${CONTAINER_NAME}:snapshot "${IMAGE_REPO}"/${CONTAINER_NAME}:snapshot
  docker push "${IMAGE_REPO}"/${CONTAINER_NAME}:snapshot
  export RELEASE_TAG=snapshot
  sed -i 's/${ContainerTAG}/'"$RELEASE_TAG"'/g' tw-teste-task-snapshot.json   
  sed -i 's/${IMAGE_REPO}/'"$IMAGE_REPO"'/g' tw-teste-task-snapshot.json             
  sed -i 's/${CONTAINER_NAME}/'"$CONTAINER_NAME"'/g' tw-teste-task-snapshot.json    
  sed -i 's/${AWS_TASK_FAMILY}/'"$AWS_TESTE_TASK_FAMILY"'/g' tw-teste-task-snapshot.json
  aws ecs register-task-definition --cli-input-json file://tw-teste-task-snapshot.json
  aws ecs update-service --cluster teste-cluster --service tec2tw --task-definition teste-tw-ec2
else 
    echo "Deploy em ambiente de Teste não realizado - Rotina de Rollback em execução."
fi